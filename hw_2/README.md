# MD2

1. Izveidojiet .NET ietvara programmatūras lietotni izmantojot Visual Studio 2010 un grafisko elementu attēlošanas rīku;
2. Izdomāt kādu tautsaimniecības problēmu, kuru var atrisināt izmantojot mūsdienu tehnoloģijas, konkrētāk, izveidojot Microsoft Form lietotni, piemēram, metālapstrādes uzņēmumam nepieciešams izveidot lietotni, kas spētu, ka tajā ievada dažāda metāla laukumus, svaru un metāla veidu un automātiski aprēķina to uzpirkšanas cena, šādus aprēķinus veic konkrētam metāla atgriezumam, kā arī visiem kopā. Jūsu izvēlēto problēmu apraksties ne vairāk kā 5-7 teikumos;
3. Izmantojot grafisko elementu attēlošanas rīku, piemēram, „Paint”, uzzīmējiet shematiski, kā šāda lietotne varētu izskatīties;
4. Izveidojiet Visual Studio 2010 Windows Form lietotni, kas realizē shematisko programmatūras shēmu, nedomājot par funkcionalitāti (par to domāsim nākamajā mājas darbā);
5. Lietotnē, katram izveidotam Toolbox elementam, jāizmanto properties logs, un jāmaina tā teksts, un elementa nosaukums uz loģiskiem nosaukumiem, piemēram, Name properties no textBox1 uz inputName, kā arī pogām Text no button1 uz Aprēķināt, pogas Name no button1 uz pogaIevadit;
6. Lietotnē izmantot vismaz 6 Toolbox elementus. tajā skaitā, poga, textBox, un menuStrip un 2 Windows Form logus;

# Projekts

## Problēma, jeb *User story*

*Klientiem nepieciešams aizdevuma amortizācijas kalkulātors*

## Apraksts ##

**Amortizācijas kalkulātoram ir jāspēj aprēķināt šādas vērtības:**
- Aizdevuma apjomu
- Aizdevuma apmaksasas ikmēneša summa
- Aizdevuma apmaksas periods
- Ikmēneša maksājuma likme

Lai aprēķinātu jubkuru no augšminētajiem rezultātiem, lietotājam ir jābūt iespējai norādīt atlikušos trīs.
Piemēram: Lai aprēķinātu aizdevuma apjomu, ir jāzin ikmēneša maksājums, vēlamais apmaksas periods un likme.

**Funkcionālās prasības:**

- Iespējai izdarīta augšminētos aprēķinus
- Ivēlnē jābūt sekojošiem priekšmetiem
	* Help - About
	* File - Save results to file

