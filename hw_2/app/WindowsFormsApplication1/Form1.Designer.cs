﻿namespace WindowsFormsApplication1
{
    partial class frmCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLoanAmount = new System.Windows.Forms.TextBox();
            this.txtLoanTerm = new System.Windows.Forms.TextBox();
            this.txtInterestRate = new System.Windows.Forms.TextBox();
            this.txtMonthlyPayment = new System.Windows.Forms.TextBox();
            this.lblLoanAmount = new System.Windows.Forms.Label();
            this.lblLoanTerm = new System.Windows.Forms.Label();
            this.lblInterestRate = new System.Windows.Forms.Label();
            this.lblMonthlyPayment = new System.Windows.Forms.Label();
            this.cboLoanTermUnit = new System.Windows.Forms.ComboBox();
            this.lblPercentPerYear = new System.Windows.Forms.Label();
            this.lblMonthlyPaymentPostfix = new System.Windows.Forms.Label();
            this.cboCalculationType = new System.Windows.Forms.ComboBox();
            this.lblCalculationType = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnGenerateExample = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveResultsToFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnResult = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtLoanAmount
            // 
            this.txtLoanAmount.Location = new System.Drawing.Point(310, 156);
            this.txtLoanAmount.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtLoanAmount.Name = "txtLoanAmount";
            this.txtLoanAmount.Size = new System.Drawing.Size(196, 31);
            this.txtLoanAmount.TabIndex = 0;
            // 
            // txtLoanTerm
            // 
            this.txtLoanTerm.Location = new System.Drawing.Point(310, 206);
            this.txtLoanTerm.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtLoanTerm.Name = "txtLoanTerm";
            this.txtLoanTerm.Size = new System.Drawing.Size(196, 31);
            this.txtLoanTerm.TabIndex = 1;
            // 
            // txtInterestRate
            // 
            this.txtInterestRate.Location = new System.Drawing.Point(310, 256);
            this.txtInterestRate.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtInterestRate.Name = "txtInterestRate";
            this.txtInterestRate.Size = new System.Drawing.Size(196, 31);
            this.txtInterestRate.TabIndex = 2;
            // 
            // txtMonthlyPayment
            // 
            this.txtMonthlyPayment.Enabled = false;
            this.txtMonthlyPayment.Location = new System.Drawing.Point(310, 306);
            this.txtMonthlyPayment.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtMonthlyPayment.Name = "txtMonthlyPayment";
            this.txtMonthlyPayment.Size = new System.Drawing.Size(196, 31);
            this.txtMonthlyPayment.TabIndex = 3;
            // 
            // lblLoanAmount
            // 
            this.lblLoanAmount.AutoSize = true;
            this.lblLoanAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanAmount.Location = new System.Drawing.Point(30, 161);
            this.lblLoanAmount.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblLoanAmount.Name = "lblLoanAmount";
            this.lblLoanAmount.Size = new System.Drawing.Size(210, 26);
            this.lblLoanAmount.TabIndex = 4;
            this.lblLoanAmount.Text = "Loan Amount ( € ):";
            this.lblLoanAmount.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblLoanTerm
            // 
            this.lblLoanTerm.AutoSize = true;
            this.lblLoanTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanTerm.Location = new System.Drawing.Point(30, 211);
            this.lblLoanTerm.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblLoanTerm.Name = "lblLoanTerm";
            this.lblLoanTerm.Size = new System.Drawing.Size(132, 26);
            this.lblLoanTerm.TabIndex = 5;
            this.lblLoanTerm.Text = "Loan Term:";
            // 
            // lblInterestRate
            // 
            this.lblInterestRate.AutoSize = true;
            this.lblInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterestRate.Location = new System.Drawing.Point(30, 261);
            this.lblInterestRate.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblInterestRate.Name = "lblInterestRate";
            this.lblInterestRate.Size = new System.Drawing.Size(147, 26);
            this.lblInterestRate.TabIndex = 6;
            this.lblInterestRate.Text = "Interest rate:";
            // 
            // lblMonthlyPayment
            // 
            this.lblMonthlyPayment.AutoSize = true;
            this.lblMonthlyPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyPayment.Location = new System.Drawing.Point(30, 311);
            this.lblMonthlyPayment.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblMonthlyPayment.Name = "lblMonthlyPayment";
            this.lblMonthlyPayment.Size = new System.Drawing.Size(253, 26);
            this.lblMonthlyPayment.TabIndex = 7;
            this.lblMonthlyPayment.Text = "Monthly Payment ( € ):";
            // 
            // cboLoanTermUnit
            // 
            this.cboLoanTermUnit.FormattingEnabled = true;
            this.cboLoanTermUnit.Items.AddRange(new object[] {
            "years",
            "months"});
            this.cboLoanTermUnit.Location = new System.Drawing.Point(522, 204);
            this.cboLoanTermUnit.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cboLoanTermUnit.Name = "cboLoanTermUnit";
            this.cboLoanTermUnit.Size = new System.Drawing.Size(114, 33);
            this.cboLoanTermUnit.TabIndex = 8;
            this.cboLoanTermUnit.Text = "years";
            // 
            // lblPercentPerYear
            // 
            this.lblPercentPerYear.AutoSize = true;
            this.lblPercentPerYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercentPerYear.Location = new System.Drawing.Point(516, 261);
            this.lblPercentPerYear.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblPercentPerYear.Name = "lblPercentPerYear";
            this.lblPercentPerYear.Size = new System.Drawing.Size(127, 26);
            this.lblPercentPerYear.TabIndex = 9;
            this.lblPercentPerYear.Text = "% per year";
            this.lblPercentPerYear.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // lblMonthlyPaymentPostfix
            // 
            this.lblMonthlyPaymentPostfix.AutoSize = true;
            this.lblMonthlyPaymentPostfix.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyPaymentPostfix.Location = new System.Drawing.Point(522, 311);
            this.lblMonthlyPaymentPostfix.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblMonthlyPaymentPostfix.Name = "lblMonthlyPaymentPostfix";
            this.lblMonthlyPaymentPostfix.Size = new System.Drawing.Size(119, 26);
            this.lblMonthlyPaymentPostfix.TabIndex = 10;
            this.lblMonthlyPaymentPostfix.Text = "per month";
            this.lblMonthlyPaymentPostfix.Click += new System.EventHandler(this.lblMonthlyPaymentPostfix_Click);
            // 
            // cboCalculationType
            // 
            this.cboCalculationType.FormattingEnabled = true;
            this.cboCalculationType.Items.AddRange(new object[] {
            "Payment Amount ( € )",
            "Loan Amount ( € )",
            "Loan Term",
            "Interest rate ( % )"});
            this.cboCalculationType.Location = new System.Drawing.Point(310, 79);
            this.cboCalculationType.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cboCalculationType.Name = "cboCalculationType";
            this.cboCalculationType.Size = new System.Drawing.Size(326, 33);
            this.cboCalculationType.TabIndex = 11;
            this.cboCalculationType.Text = "Payment Amount ( € )";
            this.cboCalculationType.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblCalculationType
            // 
            this.lblCalculationType.AutoSize = true;
            this.lblCalculationType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalculationType.Location = new System.Drawing.Point(30, 85);
            this.lblCalculationType.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblCalculationType.Name = "lblCalculationType";
            this.lblCalculationType.Size = new System.Drawing.Size(217, 26);
            this.lblCalculationType.TabIndex = 12;
            this.lblCalculationType.Text = "I Want to calculate:";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(36, 394);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(262, 79);
            this.btnCalculate.TabIndex = 13;
            this.btnCalculate.Text = "Find Monthly Payments";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(374, 485);
            this.btnReset.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(262, 79);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnGenerateExample
            // 
            this.btnGenerateExample.Location = new System.Drawing.Point(374, 394);
            this.btnGenerateExample.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnGenerateExample.Name = "btnGenerateExample";
            this.btnGenerateExample.Size = new System.Drawing.Size(262, 79);
            this.btnGenerateExample.TabIndex = 15;
            this.btnGenerateExample.Text = "Generate Example";
            this.btnGenerateExample.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(12, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(654, 46);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveResultsToFileToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveResultsToFileToolStripMenuItem
            // 
            this.saveResultsToFileToolStripMenuItem.Name = "saveResultsToFileToolStripMenuItem";
            this.saveResultsToFileToolStripMenuItem.Size = new System.Drawing.Size(326, 38);
            this.saveResultsToFileToolStripMenuItem.Text = "Export results to file";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(309, 38);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(179, 38);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // btnResult
            // 
            this.btnResult.Location = new System.Drawing.Point(36, 485);
            this.btnResult.Margin = new System.Windows.Forms.Padding(6);
            this.btnResult.Name = "btnResult";
            this.btnResult.Size = new System.Drawing.Size(262, 79);
            this.btnResult.TabIndex = 17;
            this.btnResult.Text = "View Result";
            this.btnResult.UseVisualStyleBackColor = true;
            this.btnResult.Click += new System.EventHandler(this.btnResult_Click);
            // 
            // frmCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 586);
            this.Controls.Add(this.btnResult);
            this.Controls.Add(this.btnGenerateExample);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.lblCalculationType);
            this.Controls.Add(this.cboCalculationType);
            this.Controls.Add(this.lblMonthlyPaymentPostfix);
            this.Controls.Add(this.lblPercentPerYear);
            this.Controls.Add(this.cboLoanTermUnit);
            this.Controls.Add(this.lblMonthlyPayment);
            this.Controls.Add(this.lblInterestRate);
            this.Controls.Add(this.lblLoanTerm);
            this.Controls.Add(this.lblLoanAmount);
            this.Controls.Add(this.txtMonthlyPayment);
            this.Controls.Add(this.txtInterestRate);
            this.Controls.Add(this.txtLoanTerm);
            this.Controls.Add(this.txtLoanAmount);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "frmCalculator";
            this.Text = "Loan amortization calculator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLoanAmount;
        private System.Windows.Forms.TextBox txtLoanTerm;
        private System.Windows.Forms.TextBox txtInterestRate;
        private System.Windows.Forms.TextBox txtMonthlyPayment;
        private System.Windows.Forms.Label lblLoanAmount;
        private System.Windows.Forms.Label lblLoanTerm;
        private System.Windows.Forms.Label lblInterestRate;
        private System.Windows.Forms.Label lblMonthlyPayment;
        private System.Windows.Forms.ComboBox cboLoanTermUnit;
        private System.Windows.Forms.Label lblPercentPerYear;
        private System.Windows.Forms.Label lblMonthlyPaymentPostfix;
        private System.Windows.Forms.ComboBox cboCalculationType;
        private System.Windows.Forms.Label lblCalculationType;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnGenerateExample;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveResultsToFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button btnResult;
    }
}

