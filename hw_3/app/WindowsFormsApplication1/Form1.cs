﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class frmCalculator : Form
    {
        // Constants

        private readonly string[] CalcTypes = { "Payment Amount( € )", "Loan Amount( € )", "Loan Term", "Interest rate( % )" };
        private readonly string[] LoanTermTypes = { "Years", "Months" };

        // Example constants

        private const int LoanAmount = 1000;
        private const int LoanTerm = 5;
        private const int InterestRatePrcPerYear = 7;
        private const int MonthlyPayment = 200;

        public frmCalculator()
        {
            InitializeComponent();

            // Assign the calc type combo box values
            this.cboCalculationType.Items.AddRange(CalcTypes);
            this.cboCalculationType.SelectedIndex = 0;

            // Assign the loan term combo box values
            this.cboLoanTermUnit.Items.AddRange(LoanTermTypes);
            this.cboLoanTermUnit.SelectedIndex = 0;

        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {

            Calculation calc = new Calculation(
                cboCalculationType.SelectedIndex,
                string.IsNullOrEmpty(txtLoanAmount.Text) ? 0 : int.Parse(txtLoanAmount.Text),
                string.IsNullOrEmpty(txtLoanTerm.Text) ? 0 : int.Parse(txtLoanTerm.Text),
                string.IsNullOrEmpty(txtInterestRate.Text) ? 0 : int.Parse(txtInterestRate.Text),
                string.IsNullOrEmpty(txtMonthlyPayment.Text) ? 0 : int.Parse(txtMonthlyPayment.Text)
                );

            calc.OnCalcFinished += Calc_OnCalcFinished;

            calc.calculate();
        }

        private void Calc_OnCalcFinished(object sender, EventArgs e)
        {
            MessageBox.Show("Calculation process has been completed! You can now use the result button.");
            this.btnResult.Enabled = true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtMonthlyPayment.Text = "";
            this.txtLoanAmount.Text = "";
            this.txtLoanTerm.Text = "";
            this.txtInterestRate.Text = "";
        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            frmResult frmResult = new frmResult();
            frmResult.Show();

            this.Hide();
        }

        private void btnGenerateExample_Click(object sender, EventArgs e)
        {
            this.txtLoanAmount.Text = LoanAmount.ToString();
            this.txtLoanTerm.Text = LoanTerm.ToString();
            this.txtInterestRate.Text = InterestRatePrcPerYear.ToString();
            this.txtMonthlyPayment.Text = MonthlyPayment.ToString();

            switch (this.cboCalculationType.SelectedIndex)
            {
                case 0:
                    this.txtMonthlyPayment.Text = "";
                    break;
                case 1:
                    this.txtLoanAmount.Text = "";
                    break;
                case 2:
                    this.txtLoanTerm.Text = "";
                    break;
                case 3:
                    this.txtInterestRate.Text = "";
                    break;
            }
        }

        private void cboLoanTermUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboCalculationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtMonthlyPayment.Enabled = true;
            this.txtLoanAmount.Enabled = true;
            this.txtLoanTerm.Enabled = true;
            this.txtInterestRate.Enabled = true;

            this.btnReset_Click(this.btnReset, e);

            switch (this.cboCalculationType.SelectedIndex)
            {
                case 0:
                    this.txtMonthlyPayment.Enabled = false;
                    break;
                case 1:
                    this.txtLoanAmount.Enabled = false;
                    break;
                case 2:
                    this.txtLoanTerm.Enabled = false;
                    break;
                case 3:
                    this.txtInterestRate.Enabled = false;
                    break;
            }

        }
    }
    }
