﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class frmResult : Form
    {
        public frmResult()
        {
            InitializeComponent();
        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            this.Close();

            Form frmCalculator = new frmCalculator();
            frmCalculator.Show();

            this.Hide();
        }

        public string txtResultValue
        {
            set { rtbResult.Text = value; }
        }
    }
}
