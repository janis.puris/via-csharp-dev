﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public class Calculation
    {

        public event EventHandler OnCalcFinished;

        private int _calcType;
        private int _loanAmount;
        private int _loanTerm;
        private int _interestRate;
        private int _monthlyPayment;

        public string TextDescription
        {
            get
            {
                return "Calculation class parameters:" + "\n" +
              "Calculation type ID: " + _calcType + "\n" +
              "Loan amount: " + _loanAmount + "\n" +
              "Loan term: " + _loanTerm + "\n" +
              "Interest rate: " + _interestRate + "\n" +
              "Monthly payment: " + _monthlyPayment; ;
            }
        }

        public Calculation(int calculationTypeId, int loanAmount, int loanTerm, int interestRate, int monthlyPayment)
        {
            _calcType = calculationTypeId;
            _loanAmount = loanAmount;
            _loanTerm = loanTerm;
            _interestRate = interestRate;
            _monthlyPayment = monthlyPayment;
        }

        public void calculate()
        {

            // calc code

            OnCalcFinished(this, new EventArgs());
        }
    }
}
