Vidzemes Augstskola
Inženierzinātņu fakultāte
Programmēšana II

# MD3

1.	Izveidojiet .NET ietvara programmatūras lietotni izmantojot Visual Studio 2010 un grafisko elementu attēlošanas rīku;
2.	Papildināt otro mājas darbu un pievienot lietotnei konstantes un konstanšu grupu, masīvu vai masīvu sarakstu kolekcijas, kā arī izveidot savu notikumu un delegāciju (pa priekšstatu var izmantot trešo laboratorijas darbu);
3.	Izveidot komponenti, kas implementē un izmanto iepriekšējā nosacījumā nepieciešamās prasības;
4.	Loģiski nodrošināt, ka visas šīs komponentes strādā lietotnei nepieciešamo funkcionalitātes veikšanai;
5.	Izmantot šīs komponentes un tās nodemonstrēt, bet nav jāuztraucas, ka netiek nodrošināta pilnīga lietotnes darbība, jo lietotne tiks uzlabota nākamajā mājas darbā;
6.	Ja tas ir nepieciešams, lietotni var papildināt ar papildus funkcionalitāti;
