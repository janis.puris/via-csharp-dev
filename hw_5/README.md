Vidzemes Augstskola
Inženierzinātņu fakultāte
Programmēšana II

# MD5

1.	Izveidojiet .NET ietvara programmatūras lietotni izmantojot Visual Studio 2010 un grafisko elementu attēlošanas rīku;
2.	Papildiniet ceturto mājas darbu ar papildus funkcionalitātēm, lai lietotnes spētu veikt savu mērķus;
3.	Papildiniet lietotnes metodes izmantojot izņēmumu apstrādes principus, lai izņēmumam iestājoties, lietotne nepārtrauktu darbu, bet turpinātu strādāt;
4.	Izveidojiet atsevišķu klase ar loģisku nosaukumu UNIT testu veikšanai (nav nepieciešams veidot jaunu projektu UNIT testiem, izveidojiet paši savus jaunajā klasē);
5.	UNIT testu scenārijiem ir jāpārbauda galvenās sistēmas darbības ar mērķi, vai sistēma darbojas pareizi un loģiski;
6.	Izveidot testu aprakstus (tas var būt word dokuments, excel tabula, utt.), lai vizuāli redzētu testa identifikatoru (lai saprastu, pa kuru testu iet runa), testa mērķi, testa scenāriju, sagaidāmo un faktisko rezultātu.
7.	Jābūt izveidotiem vismaz 6-10 testiem;
8.	Augstāku atzīmei, tiks vērtēts testa atbilstība sistēmas darbības pārbaudes mērķiem;
9.	Nav jāveido visi testi, tikai galvenie, kas pārbauda būtiskas lietas, piemēram, objekta dažādu parametra aprēķini, mērījumi un citu galveno sistēmu funkciju veikšanu. Nav jāpārbauda, piemēram, vai var ievadīt datus teksta laukā, vai arī nevar, tā tad dažāda ievades sīkumus;
