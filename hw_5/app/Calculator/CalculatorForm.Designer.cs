﻿namespace Calculator
{
    partial class CalculatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLoanAmount = new System.Windows.Forms.TextBox();
            this.txtLoanTerm = new System.Windows.Forms.TextBox();
            this.txtInterestRate = new System.Windows.Forms.TextBox();
            this.txtMonthlyPayment = new System.Windows.Forms.TextBox();
            this.lblLoanAmount = new System.Windows.Forms.Label();
            this.lblLoanTerm = new System.Windows.Forms.Label();
            this.lblInterestRate = new System.Windows.Forms.Label();
            this.lblMonthlyPayment = new System.Windows.Forms.Label();
            this.cboLoanTermUnit = new System.Windows.Forms.ComboBox();
            this.lblPercentPerYear = new System.Windows.Forms.Label();
            this.lblMonthlyPaymentPostfix = new System.Windows.Forms.Label();
            this.cboCalculationType = new System.Windows.Forms.ComboBox();
            this.lblCalculationType = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnGenerateExample = new System.Windows.Forms.Button();
            this.mnuCalculator = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveResultsToFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowSolution = new System.Windows.Forms.Button();
            this.mnuCalculator.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtLoanAmount
            // 
            this.txtLoanAmount.Location = new System.Drawing.Point(284, 150);
            this.txtLoanAmount.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtLoanAmount.Name = "txtLoanAmount";
            this.txtLoanAmount.Size = new System.Drawing.Size(180, 29);
            this.txtLoanAmount.TabIndex = 0;
            this.txtLoanAmount.TextChanged += new System.EventHandler(this.isNumericTextInput);
            this.txtLoanAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.isNumericTextInput);
            // 
            // txtLoanTerm
            // 
            this.txtLoanTerm.Location = new System.Drawing.Point(284, 198);
            this.txtLoanTerm.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtLoanTerm.Name = "txtLoanTerm";
            this.txtLoanTerm.Size = new System.Drawing.Size(180, 29);
            this.txtLoanTerm.TabIndex = 1;
            this.txtLoanTerm.TextChanged += new System.EventHandler(this.isNumericTextInput);
            this.txtLoanTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.isNumericTextInput);
            // 
            // txtInterestRate
            // 
            this.txtInterestRate.Location = new System.Drawing.Point(284, 246);
            this.txtInterestRate.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtInterestRate.Name = "txtInterestRate";
            this.txtInterestRate.Size = new System.Drawing.Size(180, 29);
            this.txtInterestRate.TabIndex = 2;
            this.txtInterestRate.TextChanged += new System.EventHandler(this.isNumericTextInput);
            this.txtInterestRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.isNumericTextInput);
            // 
            // txtMonthlyPayment
            // 
            this.txtMonthlyPayment.Enabled = false;
            this.txtMonthlyPayment.Location = new System.Drawing.Point(284, 294);
            this.txtMonthlyPayment.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtMonthlyPayment.Name = "txtMonthlyPayment";
            this.txtMonthlyPayment.Size = new System.Drawing.Size(180, 29);
            this.txtMonthlyPayment.TabIndex = 3;
            this.txtMonthlyPayment.TextChanged += new System.EventHandler(this.isNumericTextInput);
            this.txtMonthlyPayment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.isNumericTextInput);
            // 
            // lblLoanAmount
            // 
            this.lblLoanAmount.AutoSize = true;
            this.lblLoanAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanAmount.Location = new System.Drawing.Point(29, 155);
            this.lblLoanAmount.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblLoanAmount.Name = "lblLoanAmount";
            this.lblLoanAmount.Size = new System.Drawing.Size(193, 25);
            this.lblLoanAmount.TabIndex = 4;
            this.lblLoanAmount.Text = "Loan Amount ( € ):";
            // 
            // lblLoanTerm
            // 
            this.lblLoanTerm.AutoSize = true;
            this.lblLoanTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanTerm.Location = new System.Drawing.Point(29, 203);
            this.lblLoanTerm.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblLoanTerm.Name = "lblLoanTerm";
            this.lblLoanTerm.Size = new System.Drawing.Size(123, 25);
            this.lblLoanTerm.TabIndex = 5;
            this.lblLoanTerm.Text = "Loan Term:";
            // 
            // lblInterestRate
            // 
            this.lblInterestRate.AutoSize = true;
            this.lblInterestRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterestRate.Location = new System.Drawing.Point(29, 251);
            this.lblInterestRate.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblInterestRate.Name = "lblInterestRate";
            this.lblInterestRate.Size = new System.Drawing.Size(134, 25);
            this.lblInterestRate.TabIndex = 6;
            this.lblInterestRate.Text = "Interest rate:";
            // 
            // lblMonthlyPayment
            // 
            this.lblMonthlyPayment.AutoSize = true;
            this.lblMonthlyPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyPayment.Location = new System.Drawing.Point(29, 299);
            this.lblMonthlyPayment.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblMonthlyPayment.Name = "lblMonthlyPayment";
            this.lblMonthlyPayment.Size = new System.Drawing.Size(231, 25);
            this.lblMonthlyPayment.TabIndex = 7;
            this.lblMonthlyPayment.Text = "Monthly Payment ( € ):";
            // 
            // cboLoanTermUnit
            // 
            this.cboLoanTermUnit.Location = new System.Drawing.Point(480, 196);
            this.cboLoanTermUnit.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.cboLoanTermUnit.Name = "cboLoanTermUnit";
            this.cboLoanTermUnit.Size = new System.Drawing.Size(105, 32);
            this.cboLoanTermUnit.TabIndex = 8;
            this.cboLoanTermUnit.SelectedIndexChanged += new System.EventHandler(this.cboLoanTermUnit_SelectedIndexChanged);
            // 
            // lblPercentPerYear
            // 
            this.lblPercentPerYear.AutoSize = true;
            this.lblPercentPerYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercentPerYear.Location = new System.Drawing.Point(473, 251);
            this.lblPercentPerYear.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblPercentPerYear.Name = "lblPercentPerYear";
            this.lblPercentPerYear.Size = new System.Drawing.Size(116, 25);
            this.lblPercentPerYear.TabIndex = 9;
            this.lblPercentPerYear.Text = "% per year";
            // 
            // lblMonthlyPaymentPostfix
            // 
            this.lblMonthlyPaymentPostfix.AutoSize = true;
            this.lblMonthlyPaymentPostfix.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthlyPaymentPostfix.Location = new System.Drawing.Point(480, 299);
            this.lblMonthlyPaymentPostfix.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblMonthlyPaymentPostfix.Name = "lblMonthlyPaymentPostfix";
            this.lblMonthlyPaymentPostfix.Size = new System.Drawing.Size(108, 25);
            this.lblMonthlyPaymentPostfix.TabIndex = 10;
            this.lblMonthlyPaymentPostfix.Text = "per month";
            // 
            // cboCalculationType
            // 
            this.cboCalculationType.FormattingEnabled = true;
            this.cboCalculationType.Location = new System.Drawing.Point(284, 76);
            this.cboCalculationType.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.cboCalculationType.Name = "cboCalculationType";
            this.cboCalculationType.Size = new System.Drawing.Size(299, 32);
            this.cboCalculationType.TabIndex = 11;
            this.cboCalculationType.SelectedIndexChanged += new System.EventHandler(this.cboCalculationType_SelectedIndexChanged);
            // 
            // lblCalculationType
            // 
            this.lblCalculationType.AutoSize = true;
            this.lblCalculationType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalculationType.Location = new System.Drawing.Point(29, 81);
            this.lblCalculationType.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblCalculationType.Name = "lblCalculationType";
            this.lblCalculationType.Size = new System.Drawing.Size(198, 25);
            this.lblCalculationType.TabIndex = 12;
            this.lblCalculationType.Text = "I Want to calculate:";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(33, 378);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(240, 76);
            this.btnCalculate.TabIndex = 13;
            this.btnCalculate.Text = "Find Monthly Payments";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(343, 465);
            this.btnReset.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(240, 76);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.resetCalculator);
            // 
            // btnGenerateExample
            // 
            this.btnGenerateExample.Location = new System.Drawing.Point(343, 378);
            this.btnGenerateExample.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnGenerateExample.Name = "btnGenerateExample";
            this.btnGenerateExample.Size = new System.Drawing.Size(240, 76);
            this.btnGenerateExample.TabIndex = 15;
            this.btnGenerateExample.Text = "Generate Example";
            this.btnGenerateExample.UseVisualStyleBackColor = true;
            this.btnGenerateExample.Click += new System.EventHandler(this.btnGenerateExample_Click);
            // 
            // mnuCalculator
            // 
            this.mnuCalculator.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.mnuCalculator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mnuCalculator.Location = new System.Drawing.Point(0, 0);
            this.mnuCalculator.Name = "mnuCalculator";
            this.mnuCalculator.Padding = new System.Windows.Forms.Padding(11, 4, 0, 4);
            this.mnuCalculator.Size = new System.Drawing.Size(601, 42);
            this.mnuCalculator.TabIndex = 16;
            this.mnuCalculator.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveResultsToFileToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(56, 34);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveResultsToFileToolStripMenuItem
            // 
            this.saveResultsToFileToolStripMenuItem.Name = "saveResultsToFileToolStripMenuItem";
            this.saveResultsToFileToolStripMenuItem.Size = new System.Drawing.Size(288, 34);
            this.saveResultsToFileToolStripMenuItem.Text = "Export results to file";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(288, 34);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(68, 34);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(161, 34);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // btnShowSolution
            // 
            this.btnShowSolution.Enabled = false;
            this.btnShowSolution.Location = new System.Drawing.Point(33, 465);
            this.btnShowSolution.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnShowSolution.Name = "btnShowSolution";
            this.btnShowSolution.Size = new System.Drawing.Size(240, 76);
            this.btnShowSolution.TabIndex = 17;
            this.btnShowSolution.Text = "Show Solution";
            this.btnShowSolution.UseVisualStyleBackColor = true;
            this.btnShowSolution.Click += new System.EventHandler(this.btnShowSolution_Click);
            // 
            // CalculatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 563);
            this.Controls.Add(this.btnShowSolution);
            this.Controls.Add(this.btnGenerateExample);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.lblCalculationType);
            this.Controls.Add(this.cboCalculationType);
            this.Controls.Add(this.lblMonthlyPaymentPostfix);
            this.Controls.Add(this.lblPercentPerYear);
            this.Controls.Add(this.cboLoanTermUnit);
            this.Controls.Add(this.lblMonthlyPayment);
            this.Controls.Add(this.lblInterestRate);
            this.Controls.Add(this.lblLoanTerm);
            this.Controls.Add(this.lblLoanAmount);
            this.Controls.Add(this.txtMonthlyPayment);
            this.Controls.Add(this.txtInterestRate);
            this.Controls.Add(this.txtLoanTerm);
            this.Controls.Add(this.txtLoanAmount);
            this.Controls.Add(this.mnuCalculator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "CalculatorForm";
            this.Text = "Loan amortization calculator";
            this.mnuCalculator.ResumeLayout(false);
            this.mnuCalculator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLoanAmount;
        private System.Windows.Forms.TextBox txtLoanTerm;
        private System.Windows.Forms.TextBox txtInterestRate;
        private System.Windows.Forms.TextBox txtMonthlyPayment;
        private System.Windows.Forms.Label lblLoanAmount;
        private System.Windows.Forms.Label lblLoanTerm;
        private System.Windows.Forms.Label lblInterestRate;
        private System.Windows.Forms.Label lblMonthlyPayment;
        private System.Windows.Forms.ComboBox cboLoanTermUnit;
        private System.Windows.Forms.Label lblPercentPerYear;
        private System.Windows.Forms.Label lblMonthlyPaymentPostfix;
        private System.Windows.Forms.ComboBox cboCalculationType;
        private System.Windows.Forms.Label lblCalculationType;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnGenerateExample;
        private System.Windows.Forms.MenuStrip mnuCalculator;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveResultsToFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button btnShowSolution;
    }
}

