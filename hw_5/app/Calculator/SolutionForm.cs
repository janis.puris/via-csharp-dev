﻿using System;
using System.Windows.Forms;

namespace Calculator
{
    public partial class SolutionForm : Form
    {
        public SolutionForm()
        {
            InitializeComponent();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Close();

            Form frmCalculator = new CalculatorForm();
            frmCalculator.Show();

            this.Hide();
        }

        public string txtResultValue
        {
            set { rtbProblem.Text = value; }
        }
    }
}
