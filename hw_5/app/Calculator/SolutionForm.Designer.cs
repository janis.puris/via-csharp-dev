﻿namespace Calculator
{
    partial class SolutionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rtbProblem = new System.Windows.Forms.RichTextBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.txtProblemLabel = new System.Windows.Forms.Label();
            this.txtResultLabel = new System.Windows.Forms.Label();
            this.rtbResult = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgSummary = new System.Windows.Forms.DataGridView();
            this.amortisationSolutionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amortisationSolutionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rtbProblem
            // 
            this.rtbProblem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbProblem.Location = new System.Drawing.Point(55, 83);
            this.rtbProblem.Name = "rtbProblem";
            this.rtbProblem.ReadOnly = true;
            this.rtbProblem.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtbProblem.Size = new System.Drawing.Size(1012, 105);
            this.rtbProblem.TabIndex = 0;
            this.rtbProblem.Text = "What is the monthly payment on a mortgage of $1000 with annual interest rate of 7" +
    "% that runs for 5 years.";
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(410, 1100);
            this.btnDone.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(262, 79);
            this.btnDone.TabIndex = 18;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // txtProblemLabel
            // 
            this.txtProblemLabel.AutoSize = true;
            this.txtProblemLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProblemLabel.Location = new System.Drawing.Point(449, 43);
            this.txtProblemLabel.Name = "txtProblemLabel";
            this.txtProblemLabel.Size = new System.Drawing.Size(143, 37);
            this.txtProblemLabel.TabIndex = 19;
            this.txtProblemLabel.Text = "Problem";
            // 
            // txtResultLabel
            // 
            this.txtResultLabel.AutoSize = true;
            this.txtResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultLabel.Location = new System.Drawing.Point(471, 212);
            this.txtResultLabel.Name = "txtResultLabel";
            this.txtResultLabel.Size = new System.Drawing.Size(112, 37);
            this.txtResultLabel.TabIndex = 20;
            this.txtResultLabel.Text = "Result";
            // 
            // rtbResult
            // 
            this.rtbResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResult.Location = new System.Drawing.Point(55, 252);
            this.rtbResult.Name = "rtbResult";
            this.rtbResult.ReadOnly = true;
            this.rtbResult.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtbResult.Size = new System.Drawing.Size(1012, 105);
            this.rtbResult.TabIndex = 21;
            this.rtbResult.Text = "The monthly payments is $ 19.7.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(449, 379);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 37);
            this.label1.TabIndex = 22;
            this.label1.Text = "Summary";
            // 
            // dgSummary
            // 
            this.dgSummary.AutoGenerateColumns = false;
            this.dgSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSummary.DataSource = this.amortisationSolutionBindingSource;
            this.dgSummary.Location = new System.Drawing.Point(55, 436);
            this.dgSummary.Name = "dgSummary";
            this.dgSummary.RowTemplate.Height = 33;
            this.dgSummary.Size = new System.Drawing.Size(1012, 655);
            this.dgSummary.TabIndex = 23;
            // 
            // SolutionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 1206);
            this.Controls.Add(this.dgSummary);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtbResult);
            this.Controls.Add(this.txtResultLabel);
            this.Controls.Add(this.txtProblemLabel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.rtbProblem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "SolutionForm";
            this.Text = "Solution";
            ((System.ComponentModel.ISupportInitialize)(this.dgSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amortisationSolutionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbProblem;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Label txtProblemLabel;
        private System.Windows.Forms.Label txtResultLabel;
        private System.Windows.Forms.RichTextBox rtbResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgSummary;
        private System.Windows.Forms.BindingSource amortisationSolutionBindingSource;
    }
}