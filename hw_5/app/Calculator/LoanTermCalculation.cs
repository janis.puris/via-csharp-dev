﻿using System;

namespace Calculator
{
    // Inheriting from Calculation class
    public class LoanTermCalculation : Calculation, ICalculatable
    {
        public new int LoanTerm { get; private set; }

        public void Calculate()
        {
            // Local helper vars
            double _monthlyInterestRate;

            // Calculate the helper vars with helper methods
            _monthlyInterestRate = CalculateMonthlyRate(InterestRate);

            // Calculate Mothly payment
            this.LoanTerm = CalculateLoanTerm(_monthlyInterestRate, MonthlyPayment, LoanAmount);
        }

        public double Result()
        {
            return LoanTerm;
        }
    }
}
