﻿using System;
using System.Windows.Forms;

namespace Calculator
{
    public partial class CalculatorForm : Form
    {
        // Constants

        private readonly string[] CalcTypes = { "Payment Amount (€)", "Loan Amount (€)", "Loan Term", "Interest rate (%)" };
        private readonly string[] LoanTermTypes = { "Years", "Months" };

        // Generate example constants

        private const int LoanAmount = 1000;
        private const int LoanTerm = 5;
        private const int InterestRatePrcPerYear = 7;
        private const int MonthlyPayment = 200;

        public CalculatorForm()
        {
            InitializeComponent();

            // Assign the calc type combo box values
            this.cboCalculationType.Items.AddRange(CalcTypes);
            this.cboCalculationType.SelectedIndex = 0;

            // Assign the loan term combo box values
            this.cboLoanTermUnit.Items.AddRange(LoanTermTypes);
            this.cboLoanTermUnit.SelectedIndex = 0;

        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {

            // Check which calculation is to be done
            switch(cboCalculationType.SelectedIndex)
            {
                // Payment Amount (€)
                case 0:
                    {
                        MonthlyPaymentCalculation monthlyPaymentCalculation = new MonthlyPaymentCalculation();
                        monthlyPaymentCalculation.InterestRate = string.IsNullOrEmpty(txtInterestRate.Text) ? 0 : int.Parse(txtInterestRate.Text);
                        monthlyPaymentCalculation.LoanAmount = string.IsNullOrEmpty(txtLoanAmount.Text) ? 0 : int.Parse(txtLoanAmount.Text);
                        monthlyPaymentCalculation.LoanTerm = string.IsNullOrEmpty(txtLoanTerm.Text) ? 0 : int.Parse(txtLoanTerm.Text);
                        monthlyPaymentCalculation.IsLoanTermMonthly = Convert.ToBoolean(cboLoanTermUnit.SelectedIndex);

                        // Actual calculation
                        monthlyPaymentCalculation.Calculate();

                        // Fill in the appropriate text box
                        txtMonthlyPayment.Text = monthlyPaymentCalculation.Result().ToString();
                        break;
                    }

                // Loan Amount (€)
                case 1:
                    { break; }

                // Loan Term
                case 2:
                    { break; }

                // Interest rate (%)
                case 3:
                    { break; }

            }

            // Calculation is done, enable the solution button
            this.btnShowSolution.Enabled = true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            // Reset all input fields
            this.txtMonthlyPayment.Text = "";
            this.txtLoanAmount.Text = "";
            this.txtLoanTerm.Text = "";
            this.txtInterestRate.Text = "";

            this.btnShowSolution.Enabled = false;
        }

        private void btnShowSolution_Click(object sender, EventArgs e)
        {
            // Init Solution form and show it
            SolutionForm frmSolution = new SolutionForm();
            frmSolution.Show();

            // Hide Calculation form
            this.Hide();
        }

        private void btnGenerateExample_Click(object sender, EventArgs e)
        {

            // Fill in the text boxes with constants
            this.txtLoanAmount.Text = LoanAmount.ToString();
            this.txtLoanTerm.Text = LoanTerm.ToString();
            this.txtInterestRate.Text = InterestRatePrcPerYear.ToString();
            this.txtMonthlyPayment.Text = MonthlyPayment.ToString();

            // Remove the constant from input text box that we are going to calc
            switch (this.cboCalculationType.SelectedIndex)
            {
                case 0:
                    this.txtMonthlyPayment.Text = "";
                    break;
                case 1:
                    this.txtLoanAmount.Text = "";
                    break;
                case 2:
                    this.txtLoanTerm.Text = "";
                    break;
                case 3:
                    this.txtInterestRate.Text = "";
                    break;
            }
        }

        // This method will be started when calculation type is changed and is perfect moment to disable the calculable value text box to prevent user input
        private void cboCalculationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtMonthlyPayment.Enabled = true;
            this.txtLoanAmount.Enabled = true;
            this.txtLoanTerm.Enabled = true;
            this.txtInterestRate.Enabled = true;

            // Lets reset the text boxes on the form. For that we will simply call the same method that subscribed to btn event
            this.btnReset_Click(this.btnReset, e);

            switch (this.cboCalculationType.SelectedIndex)
            {
                case 0:
                    this.txtMonthlyPayment.Enabled = false;
                    break;
                case 1:
                    this.txtLoanAmount.Enabled = false;
                    break;
                case 2:
                    this.txtLoanTerm.Enabled = false;
                    break;
                case 3:
                    this.txtInterestRate.Enabled = false;
                    break;
            }

        }
    }
    }
