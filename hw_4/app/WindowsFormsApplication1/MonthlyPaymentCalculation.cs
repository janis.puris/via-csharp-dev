﻿using System;

namespace Calculator
{
    // Inheriting from Calculation class
    public class MonthlyPaymentCalculation : Calculation
    {

        public override void Calculate()
        {
            // Local helper vars
            double _monthlyInterestRate;
            int _numMonthlyPayments;

            // Calculate the helper vars with helper methods
            _numMonthlyPayments = NumberOfMonthlyPayments(IsLoanTermMonthly, LoanTerm);
            _monthlyInterestRate = CalculateMonthlyRate(InterestRate);

            // Calculate Mothly payment
            MonthlyPayment = Math.Round(CalculateMonthlyPayment(_monthlyInterestRate, LoanAmount, _numMonthlyPayments),2);
        }

       // Calculation helper method
        private double CalculateMonthlyRate(int interestRate)
        {
            return Math.Pow((1 + interestRate / 100.0), (1.0 / 12.0)) - 1;
        }

        // Calculation helper method
        private double CalculateMonthlyPayment(double monthlyInterestRate, int loanAmount, int numMonthlyPayments)
        {
            return (loanAmount * monthlyInterestRate) / (1 - Math.Pow((1 + monthlyInterestRate), numMonthlyPayments * - 1));
        }

        // TYPE CHANGED. PARENT HAS THIS METHOD RETURNING INT
        // Need to remember not to use this :) this is only for school assignment
        public new double Result()
        {
            return MonthlyPayment;
        }
    }
}
