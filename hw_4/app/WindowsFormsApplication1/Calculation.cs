﻿namespace Calculator
{
    // Interfacing from ICalculatable
    public class Calculation : ICalculatable
    {
        // Implementing the properties from Interface
        public int LoanAmount { get; set; }
        public int LoanTerm { get; set; }
        public int InterestRate { get; set; }
        public bool IsLoanTermMonthly { get; set; }
        public double MonthlyPayment { get; set; }

        // Implementing method
        public int Result()
        {
            return 0;
        }

        // Implementing method
        public virtual void Calculate()
        {
        }

        // Calculation class method, this method will be inherited as is
        public int NumberOfMonthlyPayments(bool isLoanTermMonthly, int loanTermValue)
        {
            if (isLoanTermMonthly)
            {
                return loanTermValue;
            }
            else
            {
                return loanTermValue * 12;
            }
        }
    }
}
