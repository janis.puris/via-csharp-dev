﻿namespace Calculator
{
    interface ICalculatable
    {

        // Properties that will be needed for all calculations
        int LoanAmount { get; set; }
        int LoanTerm { get; set; }
        int InterestRate { get; set; }
        bool IsLoanTermMonthly { get; set; }
        double MonthlyPayment { get; set; }

        // Methods we will need for any calculation

        // Returns result
        int Result();

        // Does calculation
        void Calculate();
        
        // Make sure you have correct monthly payments
        int NumberOfMonthlyPayments(bool isLoanTermMonthly, int loanTermValue);

    }
}
