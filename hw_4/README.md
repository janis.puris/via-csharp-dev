Vidzemes Augstskola
Inženierzinātņu fakultāte
Programmēšana II

# MD4

1.	Izveidojiet .NET ietvara programmatūras lietotni izmantojot Visual Studio 2010 un grafisko elementu attēlošanas rīku;
2.	Papildiniet trešo mājas darbu un turpiniet uzlabot mājas darba lietotnes funkcionalitāti, izveidojot mantošanas principus;
3.	Mantošanas procesos, demonstrējiet vienas metodes paslēpšanu, un to izpildi lietotnē, kā arī situāciju, kad nepieciešama bāzes klases metode un izpildiet to (netiek izpildīta apslēpšana);
4.	Izveidojiet arī pārslogotas metodes, kuru darbība būtu ieviesta lietotnes darbībā;
5.	Ja tas ir nepieciešams, lietotni var papildināt ar papildus funkcionalitāti;
6.	Augstākai atzīmei – izveidojiet savu saskarni un ieviesiet to sistēmas darbībā;
7.	Lai vieglāk izprastu jūsu lietotnes funkcionalitāti, izmantojiet komentārus;
