﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MD1
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(frmMain_FormClosed);
        }

        public void LogWrite(string logMessage)
        {
            try
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
                // What do we say to death ? Not today :)
            }
        }

        private static void Log(string logMessage, TextWriter w)
        {
            w.WriteLine("{0}: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), logMessage);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LogWrite("Application started");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numX, numY, result;

            LogWrite("Button press. Button text: " + cmdPlus.ToString());

            if (int.TryParse(txtNumX.Text, out numX))
            {
                if (int.TryParse(txtNumY.Text, out numY))
                {

                    LogWrite("Valid input for both variables. X: " + txtNumX.Text + ". Y: " + txtNumY.Text);
                    result = numX + numY;
                    txtNumResult.Text = result.ToString();
                }
                else
                {
                    LogWrite("Invalid input for Y [Non-numeric or too large]. Expected numeric value, but found: " + txtNumX.ToString());
                    MessageBox.Show("Value of Y must be numeric [Non-numeric or too large]", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                LogWrite("Invalid input for X [Non-numeric or too large]. Expected numeric value, but found: " + txtNumY.ToString());
                MessageBox.Show("Value of X must be numeric [Non-numeric or too large]", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            LogWrite("Button press. Text box values have been reset to defaults.");

            txtNumX.Text = "0";
            txtNumY.Text = "0";
            txtNumResult.Text = "";
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            LogWrite("Application has been closed successfully");
        }
    }
}
