﻿namespace MD1
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumX = new System.Windows.Forms.TextBox();
            this.txtNumY = new System.Windows.Forms.TextBox();
            this.cmdPlus = new System.Windows.Forms.Button();
            this.lblNumX = new System.Windows.Forms.Label();
            this.lblNumY = new System.Windows.Forms.Label();
            this.lblNumResult = new System.Windows.Forms.Label();
            this.cmdReset = new System.Windows.Forms.Button();
            this.txtNumResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtNumX
            // 
            this.txtNumX.Location = new System.Drawing.Point(58, 33);
            this.txtNumX.MaxLength = 12;
            this.txtNumX.Name = "txtNumX";
            this.txtNumX.Size = new System.Drawing.Size(156, 20);
            this.txtNumX.TabIndex = 0;
            this.txtNumX.Text = "0";
            this.txtNumX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNumX.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtNumY
            // 
            this.txtNumY.Location = new System.Drawing.Point(58, 59);
            this.txtNumY.MaxLength = 12;
            this.txtNumY.Name = "txtNumY";
            this.txtNumY.Size = new System.Drawing.Size(156, 20);
            this.txtNumY.TabIndex = 1;
            this.txtNumY.Text = "0";
            this.txtNumY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cmdPlus
            // 
            this.cmdPlus.Location = new System.Drawing.Point(58, 111);
            this.cmdPlus.Name = "cmdPlus";
            this.cmdPlus.Size = new System.Drawing.Size(75, 23);
            this.cmdPlus.TabIndex = 2;
            this.cmdPlus.Text = "X + Y";
            this.cmdPlus.UseVisualStyleBackColor = true;
            this.cmdPlus.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblNumX
            // 
            this.lblNumX.AutoSize = true;
            this.lblNumX.Location = new System.Drawing.Point(38, 36);
            this.lblNumX.Name = "lblNumX";
            this.lblNumX.Size = new System.Drawing.Size(14, 13);
            this.lblNumX.TabIndex = 3;
            this.lblNumX.Text = "X";
            this.lblNumX.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblNumY
            // 
            this.lblNumY.AutoSize = true;
            this.lblNumY.Location = new System.Drawing.Point(38, 62);
            this.lblNumY.Name = "lblNumY";
            this.lblNumY.Size = new System.Drawing.Size(14, 13);
            this.lblNumY.TabIndex = 4;
            this.lblNumY.Text = "Y";
            // 
            // lblNumResult
            // 
            this.lblNumResult.AutoSize = true;
            this.lblNumResult.Location = new System.Drawing.Point(39, 88);
            this.lblNumResult.Name = "lblNumResult";
            this.lblNumResult.Size = new System.Drawing.Size(13, 13);
            this.lblNumResult.TabIndex = 6;
            this.lblNumResult.Text = "=";
            this.lblNumResult.Click += new System.EventHandler(this.label3_Click);
            // 
            // cmdReset
            // 
            this.cmdReset.Location = new System.Drawing.Point(139, 111);
            this.cmdReset.Name = "cmdReset";
            this.cmdReset.Size = new System.Drawing.Size(75, 23);
            this.cmdReset.TabIndex = 7;
            this.cmdReset.Text = "Reset";
            this.cmdReset.UseVisualStyleBackColor = true;
            this.cmdReset.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtNumResult
            // 
            this.txtNumResult.Enabled = false;
            this.txtNumResult.Location = new System.Drawing.Point(58, 85);
            this.txtNumResult.Name = "txtNumResult";
            this.txtNumResult.Size = new System.Drawing.Size(156, 20);
            this.txtNumResult.TabIndex = 8;
            this.txtNumResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 151);
            this.Controls.Add(this.txtNumResult);
            this.Controls.Add(this.cmdReset);
            this.Controls.Add(this.lblNumResult);
            this.Controls.Add(this.lblNumY);
            this.Controls.Add(this.lblNumX);
            this.Controls.Add(this.cmdPlus);
            this.Controls.Add(this.txtNumY);
            this.Controls.Add(this.txtNumX);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMain";
            this.Text = "MD1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNumX;
        private System.Windows.Forms.TextBox txtNumY;
        private System.Windows.Forms.Button cmdPlus;
        private System.Windows.Forms.Label lblNumX;
        private System.Windows.Forms.Label lblNumY;
        private System.Windows.Forms.Label lblNumResult;
        private System.Windows.Forms.Button cmdReset;
        private System.Windows.Forms.TextBox txtNumResult;
    }
}

