Vidzemes Augstskola
Inženierzinātņu fakultāte 2016
Programmēšana II – 2016

MD1

1. Izveidojiet .NET ietvara programmatūras lietotni izmantojot Visual Studio 2010;
2. Lietotne jāizveido balstoties uz Windows Form projekta, vienalga kādā programmēšanas valodā;
3. Atverot lietotni, studentam ir jārealizē:
a. Windows Formas logs;
b. Windows Formas logā jābūt izmantotiem vismaz diviem textBox elementiem;
c. Windows Formā jābūt redzamai pogai, kas aprēķina vienalga kādu matemātisku darbību, balstoties uz textBox ievadītajiem datiem;
4. Izpildot 3. punkta prasības, students pretendē uz 8 ballēm par mājas darbu. Augstākas atzīmes iegūšanai studentam jārealizē:
a. Izpildes laikā veidot paziņojumus log failos, kas ziņo par datu, procesa vai citiem lietotnes izpildes elementiem;
b. Log faila informācijai jābūt loģiskai un jāspēj attēlot svarīgu informāciju par lietotnes darbību un ietilpst tās loģiskajā darbībā;
c. Studentam jārealizē vismaz viena pārbaude, kas saistās ar neloģisku datu ievades apstrādi lietotnē un to rezultātu informēšanu lietotājam, piemēram, ievadot burtu skaitļu vietā, lietotne parāda paziņojuma logu lietotājam.
5. Mājas darbs jānosūta līdz 12. martam. Mājas darba kavējuma rezultātā tiks samazināta gala atzīme, attiecīgi par katrām nokavētām trim dienām, tiks atņemta viena balle;
6. Mājas darbu nosūtīt uz e-pastu gatis.blums@va.lv, norādot e-pasta „subject” jeb „temats” laukā tekstu: Vards_Uzvards_PROGII_1_majas_darbs.