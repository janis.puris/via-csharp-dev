Vidzemes Augstskola
Inženierzinātņu fakultāte
Programmēšana II

# Kursa darbs

.NET kursa kursu darba prasības – 2016
Vispārējās prasības
.NET kursa darbā studentam jāattēlo kursa laikā apgūtās prasmes. Studentam jāizveido .NET formu lietotne, kas atrisina tautsaimniecības problēmu. Students šo problēmu var brīvi izvēlēties, bet tā nedrīkst sakrist ar mājas darbos risināmo tēmu. Veidojot lietotni, studentam nav jāveic tā dokumentēšana.

.NET lietotnes prasības  
1)	Ja students veido .NET lietotni, bez atsevišķa instalēta spraudņa:  
a.	Lietotnē jābūt izmantotām vismaz divām formām;  
b.	Katrā formā jābūt vismaz septiņiem kontroles elementiem;  
c.	Lietotnei jāizmanto datu bāze;  
d.	Lietotnes koda veidošanā jāizmanto šādi objektorientētās programmēšanas paņēmieni:  
i.	Klases;  
ii.	Pārslogotās metodes;
iii.	Konstantes, konstanšu grupas, masīvi, saraksti;  
iv.	Cikli;  
v.	Metožu paslēpšana.  
e.	Lietotnei jābūt instalācijas setup izpildāmajam failam.  
2)	Ja students veido specifisku .NET lietotni (speciāls spraudnis, speciālas .NET prasības):  
a.	Par sistēmas prasībām jājautā pasniedzējam.  