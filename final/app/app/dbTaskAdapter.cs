﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;

namespace app
{
    public class dbTaskAdapter
    {

        OleDbConnection connection;
        LinkedList<TaskListItem> list;

        public dbTaskAdapter()
        {
            list = new LinkedList<TaskListItem>();
            connection = new OleDbConnection();
        }

        void close()
        {
            connection.Close();
        }

        public LinkedList<TaskListItem> selectTasks()
        {
            connect();
            OleDbCommand command = new OleDbCommand("select * from Task", connection);
            OleDbDataReader reader = command.ExecuteReader();
            
            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                bool check = reader.GetBoolean(1);
                string task = reader.GetString(2);
                string date = reader.GetDateTime(3).ToString();
                TaskListItem item = new TaskListItem(check, id, task, date);
                list.AddLast(item);
            }

            close();
            return list;
        }

        public int insertTask(TaskListItem task)
        {
            int id = 0;

            connect();
            OleDbCommand command = new OleDbCommand("insert into Task (isDone, Description, Deadline) values (?, ?, ?)", connection);
            command.Parameters.Add(new OleDbParameter("isDone", task.IsDone));
            command.Parameters.Add(new OleDbParameter("Description", task.Description));
            command.Parameters.Add(new OleDbParameter("Deadline", task.Date.ToString()));

            command.ExecuteNonQuery();

            // Get autoincrement ID
            command = new OleDbCommand("select @@IDENTITY",connection);
            OleDbDataReader reader = command.ExecuteReader();

            if (!reader.Read())
            {
                MessageBox.Show("Error: Could not get inserter record ID from database");
            } else
            {
                id = reader.GetInt32(0);
            }

            close();

            return id;
        }

        public void updateTask(TaskListItem task)
        {
            connect();

            OleDbCommand command = new OleDbCommand("update Task set isDone=@isDone, Description=@Description, Deadline=@Deadline where ID=" + task.ID.ToString(), connection);
            command.Parameters.Add(new OleDbParameter("isDone", task.IsDone));
            command.Parameters.Add(new OleDbParameter("Description", task.Description));
            command.Parameters.Add(new OleDbParameter("Deadline", task.Date.ToString()));

            if (command.ExecuteNonQuery() != 1)
            {
                MessageBox.Show("Error: The changes were not saved to DB");
            }

            close();
        }

        public void deleteTask(int taskId)
        {
            connect();

            OleDbCommand command = new OleDbCommand("delete from Task where ID=" + taskId, connection);

            if (command.ExecuteNonQuery() != 1)
            {
                MessageBox.Show("Error: The changes were not saved to DB");
            }

            close();
        }

        private void connect()
        {
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\db.accdb;Persist Security Info=False;";

            try
            {
                connection.Open();
            } catch (Exception ex)
            {
                MessageBox.Show("Error connecting to database: " + ex);
            }
        }
    }
}