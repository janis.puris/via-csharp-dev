﻿using System;
using System.Windows.Forms;

namespace app
{
    public partial class TaskForm : Form
    {
        TaskListItem item = null;
        public TaskForm(TaskListItem item)
        {
            InitializeComponent();
            this.item = item;
            UpdateFormComponents();
        }

        public void UpdateFormComponents()
        {
            rtbTaskDescription.Text = item.Description;
            txtAddTaskDeadline.Text = item.Date;
        }

        private void btnAddTaskSave_Click(object sender, EventArgs e)
        {
            DateTime result;
            if (DateTime.TryParse(txtAddTaskDeadline.Text, out result))
            {
                item.Description = rtbTaskDescription.Text;
                item.Date = txtAddTaskDeadline.Text;

                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Choose deadline in the calendar");
            }
        }

        private void calAddTaskDeadline_DateChanged(object sender, DateRangeEventArgs e)
        {
            txtAddTaskDeadline.MaxLength = 1;
            txtAddTaskDeadline.Text = calAddTaskDeadline.SelectionRange.Start.ToShortDateString();
        }
    }
}
