﻿using System;
using System.Windows.Forms;

namespace app
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TaskListForm());
            
            AppDomain.CurrentDomain.SetData("DataDirectory", Application.StartupPath);
        }
    }
}
