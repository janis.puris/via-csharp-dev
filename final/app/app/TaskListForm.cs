﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace app
{
    public partial class TaskListForm : Form
    {
        
        // Task list object
        LinkedList<TaskListItem> list = new LinkedList<TaskListItem>();

        // DB object init
        dbTaskAdapter db = new dbTaskAdapter();

        public TaskListForm()
        {
            InitializeComponent();
            list = db.selectTasks();
            refreshTaskView();
        }

        private void btnAddTask_Click(object sender, EventArgs e)
        {
            TaskListItem task = new TaskListItem(false, 0, "", "");
            TaskForm taskForm = new TaskForm(task);
            if (taskForm.ShowDialog(this) == DialogResult.OK)
            {
                // New task list object item
                ListViewItem item = new ListViewItem();

                // Pass existing list item properties to the new list item
                item.Tag = task;
                item.Checked = task.IsDone;
                item.SubItems.Add(task.Description);

                // Check if deadline exists
                if (!String.IsNullOrEmpty(task.Date))
                {
                    DateTime temp;
                    if (DateTime.TryParse(task.Date, out temp))
                    {
                        // Get specified deadline
                        DateTime dt = DateTime.Parse(task.Date);

                        // Calculate difference between the specified deadline and current datetime
                        TimeSpan dtDiff = dt - DateTime.Now;

                        // Update properties for deadline and days difference
                        item.SubItems.Add(task.Date);
                        item.SubItems.Add(dtDiff.Days.ToString());
                    }
                    // Insert the new task in db
                    task.ID = db.insertTask(task);
                }

                // Add the new item as last item on the list object in parent
                this.list.AddLast(task);

                // Add the new item to the listview object
                lvwTaskList.Items.Add(item);
                
                // Refresh listview object
                refreshTaskView();
            }
        }

        // Refresh listview object method
        public void refreshTaskView()
        {
            // Remove all entries from listview object
            lvwTaskList.Items.Clear();
            
            // Loop through all list items
            for (int i = 0; i < list.Count(); i++)
            {
                DateTime date;

                // If filter by date is enabled, then only add the items that are younger than specified date
                if (chkFilterByDate.Checked && DateTime.TryParse(list.ElementAt(i).Date, out date))
                {
                    var difference = calDeadline.SelectionRange.Start - date;
                    if (difference.TotalDays > 0)
                    {
                        continue;
                    }
                }

                // Make a new listview item and update it
                var item = new ListViewItem();
                item.Text = "";
                item.Tag = list.ElementAt(i);
                item.Checked = list.ElementAt(i).IsDone;

                item.SubItems.Add(list.ElementAt(i).Description);
                item.SubItems.Add(list.ElementAt(i).Date);

                // Update the color of the item according to its days remaining
                UpdateColors(list.ElementAt(i), item);

                // Addd the item to the listview
                lvwTaskList.Items.Add(item);
                lvwTaskList.Tag = item;
            }
        }

        private void UpdateColors(TaskListItem taskListItem, ListViewItem item)
        {
            DateTime dt;
            if (DateTime.TryParse(taskListItem.Date, out dt))
            {
                // Calculate the day difference 
                TimeSpan difference = dt - DateTime.Now;
                item.SubItems.Add(difference.Days.ToString());

                // Check if the task is done
                // Color the item accordingly
                if (item.Checked)
                {
                    item.BackColor = Color.White;
                    item.ForeColor = Color.Gray;
                }
                else
                {
                    item.ForeColor = Color.Black;
                    if (difference.TotalDays > 1)
                    {
                        item.BackColor = Color.Green;
                    }
                    else if (difference.TotalDays >= 0)
                    {
                        item.BackColor = Color.Yellow;
                    }
                    else
                    {
                        item.BackColor = Color.Red;
                    }
                }
            }
        }

        // When item is checked in the listview, we will make sure it is color updated
        private void lvwTaskList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            ListViewItem item = e.Item as ListViewItem;
            (item.Tag as TaskListItem).IsDone = item.Checked;
            UpdateColors(item.Tag as TaskListItem, item);
            int id = (item.Tag as TaskListItem).ID;
            int check = Convert.ToInt16((item.Tag as TaskListItem).IsDone);
        }

        // When filtering by date checkbox state changes, we will update its TXT propertie to have the date until
        private void chkFilterByDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterByDate.Checked)
            {
                chkFilterByDate.Text = "Filter by date until " + calDeadline.SelectionRange.Start.ToShortDateString();
            } else
            {
                chkFilterByDate.Text = "Filter by date";
            }

            // Refresh the list so we only have filtered items
            refreshTaskView();
        }

        // When date is changed or selected for first time
        private void calDeadline_DateChanged(object sender, DateRangeEventArgs e)
        {
            if (chkFilterByDate.Checked)
            {
                // Updathe the filter by date checkbox with what date we are filtering
                chkFilterByDate.Text = "Filter by date until " + e.Start.ToShortDateString();

                // Update the task list
                refreshTaskView();
            }
        }

        private void btnEditTask_Click(object sender, EventArgs e)
        {
            if (lvwTaskList.SelectedItems.Count == 1)
            {
                // Create new item and then populate it with appropriate selected data
                TaskListItem item = lvwTaskList.SelectedItems[0].Tag as TaskListItem;

                // Spawn task form
                TaskForm taskForm = new TaskForm(item);

                // When the dialog ok button has been pressed
                if (taskForm.ShowDialog(this) == DialogResult.OK)
                {
                    // Update the item properties
                    int id = item.ID;
                    int check = Convert.ToInt16(item.IsDone);
                    string task = item.Description;
                    string date = item.Date;

                    // Update the task in db
                    db.updateTask(item);

                    // Update the task in task list
                    refreshTaskView();
                }
            }
            else
            {
                MessageBox.Show("You must select the task you want to edit. (Only one)");
            }
        }

        //Delete task button
        private void btnDeleteTask_Click(object sender, EventArgs e)
        {
            // Loop through all selected items
            foreach (ListViewItem eachItem in lvwTaskList.SelectedItems)
            {
                TaskListItem task = eachItem.Tag as TaskListItem;
                list.Remove(task);
                lvwTaskList.Items.Remove(eachItem);
                db.deleteTask(task.ID);
            }
        }
    }
}
