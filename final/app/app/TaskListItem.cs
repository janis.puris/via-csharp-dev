﻿namespace app
{
    public class TaskListItem
    {
        public int ID { get; set; }
        public bool IsDone { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }

        public TaskListItem(bool _isDone, int _id, string _description, string _date)
        {
            ID = _id;
            IsDone = _isDone;
            Description = _description;
            Date = _date;
        }
    }

}
