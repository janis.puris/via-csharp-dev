﻿namespace app
{
    partial class TaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddTaskCancel = new System.Windows.Forms.Button();
            this.btnAddTaskSave = new System.Windows.Forms.Button();
            this.calAddTaskDeadline = new System.Windows.Forms.MonthCalendar();
            this.rtbTaskDescription = new System.Windows.Forms.RichTextBox();
            this.lblTask = new System.Windows.Forms.Label();
            this.lblDeadline = new System.Windows.Forms.Label();
            this.txtAddTaskDeadline = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAddTaskCancel
            // 
            this.btnAddTaskCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAddTaskCancel.Location = new System.Drawing.Point(62, 294);
            this.btnAddTaskCancel.Name = "btnAddTaskCancel";
            this.btnAddTaskCancel.Size = new System.Drawing.Size(75, 23);
            this.btnAddTaskCancel.TabIndex = 22;
            this.btnAddTaskCancel.Text = "Cancel";
            this.btnAddTaskCancel.UseVisualStyleBackColor = true;
            // 
            // btnAddTaskSave
            // 
            this.btnAddTaskSave.Location = new System.Drawing.Point(179, 294);
            this.btnAddTaskSave.Name = "btnAddTaskSave";
            this.btnAddTaskSave.Size = new System.Drawing.Size(75, 23);
            this.btnAddTaskSave.TabIndex = 21;
            this.btnAddTaskSave.Text = "Save";
            this.btnAddTaskSave.UseVisualStyleBackColor = true;
            this.btnAddTaskSave.Click += new System.EventHandler(this.btnAddTaskSave_Click);
            // 
            // calAddTaskDeadline
            // 
            this.calAddTaskDeadline.Location = new System.Drawing.Point(62, 101);
            this.calAddTaskDeadline.MaxSelectionCount = 1;
            this.calAddTaskDeadline.Name = "calAddTaskDeadline";
            this.calAddTaskDeadline.TabIndex = 20;
            this.calAddTaskDeadline.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calAddTaskDeadline_DateChanged);
            // 
            // rtbTaskDescription
            // 
            this.rtbTaskDescription.Location = new System.Drawing.Point(62, 10);
            this.rtbTaskDescription.Name = "rtbTaskDescription";
            this.rtbTaskDescription.Size = new System.Drawing.Size(194, 81);
            this.rtbTaskDescription.TabIndex = 17;
            this.rtbTaskDescription.Text = "";
            // 
            // lblTask
            // 
            this.lblTask.AutoSize = true;
            this.lblTask.Location = new System.Drawing.Point(17, 10);
            this.lblTask.Name = "lblTask";
            this.lblTask.Size = new System.Drawing.Size(34, 13);
            this.lblTask.TabIndex = 16;
            this.lblTask.Text = "Task:";
            // 
            // lblDeadline
            // 
            this.lblDeadline.AutoSize = true;
            this.lblDeadline.Location = new System.Drawing.Point(8, 101);
            this.lblDeadline.Name = "lblDeadline";
            this.lblDeadline.Size = new System.Drawing.Size(49, 13);
            this.lblDeadline.TabIndex = 18;
            this.lblDeadline.Text = "Deadline";
            // 
            // txtAddTaskDeadline
            // 
            this.txtAddTaskDeadline.Enabled = false;
            this.txtAddTaskDeadline.Location = new System.Drawing.Point(62, 273);
            this.txtAddTaskDeadline.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtAddTaskDeadline.Name = "txtAddTaskDeadline";
            this.txtAddTaskDeadline.Size = new System.Drawing.Size(194, 20);
            this.txtAddTaskDeadline.TabIndex = 23;
            // 
            // TaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 329);
            this.Controls.Add(this.txtAddTaskDeadline);
            this.Controls.Add(this.btnAddTaskCancel);
            this.Controls.Add(this.btnAddTaskSave);
            this.Controls.Add(this.calAddTaskDeadline);
            this.Controls.Add(this.lblDeadline);
            this.Controls.Add(this.rtbTaskDescription);
            this.Controls.Add(this.lblTask);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "TaskForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Task";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddTaskCancel;
        private System.Windows.Forms.Button btnAddTaskSave;
        private System.Windows.Forms.MonthCalendar calAddTaskDeadline;
        private System.Windows.Forms.RichTextBox rtbTaskDescription;
        private System.Windows.Forms.Label lblTask;
        private System.Windows.Forms.Label lblDeadline;
        private System.Windows.Forms.TextBox txtAddTaskDeadline;
    }
}