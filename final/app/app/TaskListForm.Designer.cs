﻿namespace app
{
    partial class TaskListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvwTaskList = new System.Windows.Forms.ListView();
            this.isItDone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.task = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.deadline = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.daysLeft = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chkFilterByDate = new System.Windows.Forms.CheckBox();
            this.btnAddTask = new System.Windows.Forms.Button();
            this.btnEditTask = new System.Windows.Forms.Button();
            this.btnDeleteTask = new System.Windows.Forms.Button();
            this.calDeadline = new System.Windows.Forms.MonthCalendar();
            this.SuspendLayout();
            // 
            // lvwTaskList
            // 
            this.lvwTaskList.CheckBoxes = true;
            this.lvwTaskList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.isItDone,
            this.task,
            this.deadline,
            this.daysLeft});
            this.lvwTaskList.FullRowSelect = true;
            this.lvwTaskList.GridLines = true;
            this.lvwTaskList.Location = new System.Drawing.Point(8, 8);
            this.lvwTaskList.Name = "lvwTaskList";
            this.lvwTaskList.Size = new System.Drawing.Size(523, 290);
            this.lvwTaskList.TabIndex = 6;
            this.lvwTaskList.UseCompatibleStateImageBehavior = false;
            this.lvwTaskList.View = System.Windows.Forms.View.Details;
            this.lvwTaskList.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvwTaskList_ItemChecked);
            // 
            // isItDone
            // 
            this.isItDone.Text = "Done";
            this.isItDone.Width = 38;
            // 
            // task
            // 
            this.task.Text = "Task";
            this.task.Width = 328;
            // 
            // deadline
            // 
            this.deadline.Text = "Deadline";
            this.deadline.Width = 94;
            // 
            // daysLeft
            // 
            this.daysLeft.Text = "Days left";
            this.daysLeft.Width = 57;
            // 
            // chkFilterByDate
            // 
            this.chkFilterByDate.AutoSize = true;
            this.chkFilterByDate.Location = new System.Drawing.Point(542, 181);
            this.chkFilterByDate.Name = "chkFilterByDate";
            this.chkFilterByDate.Size = new System.Drawing.Size(86, 17);
            this.chkFilterByDate.TabIndex = 13;
            this.chkFilterByDate.Text = "Filter by date";
            this.chkFilterByDate.UseVisualStyleBackColor = true;
            this.chkFilterByDate.CheckedChanged += new System.EventHandler(this.chkFilterByDate_CheckedChanged);
            // 
            // btnAddTask
            // 
            this.btnAddTask.Location = new System.Drawing.Point(542, 215);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(137, 23);
            this.btnAddTask.TabIndex = 12;
            this.btnAddTask.Text = "Add";
            this.btnAddTask.UseVisualStyleBackColor = true;
            this.btnAddTask.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // btnEditTask
            // 
            this.btnEditTask.Location = new System.Drawing.Point(542, 244);
            this.btnEditTask.Name = "btnEditTask";
            this.btnEditTask.Size = new System.Drawing.Size(137, 23);
            this.btnEditTask.TabIndex = 11;
            this.btnEditTask.Text = "Edit";
            this.btnEditTask.UseVisualStyleBackColor = true;
            this.btnEditTask.Click += new System.EventHandler(this.btnEditTask_Click);
            // 
            // btnDeleteTask
            // 
            this.btnDeleteTask.Location = new System.Drawing.Point(542, 274);
            this.btnDeleteTask.Name = "btnDeleteTask";
            this.btnDeleteTask.Size = new System.Drawing.Size(137, 23);
            this.btnDeleteTask.TabIndex = 10;
            this.btnDeleteTask.Text = "Delete";
            this.btnDeleteTask.UseVisualStyleBackColor = true;
            this.btnDeleteTask.Click += new System.EventHandler(this.btnDeleteTask_Click);
            // 
            // calDeadline
            // 
            this.calDeadline.Location = new System.Drawing.Point(542, 8);
            this.calDeadline.MaxSelectionCount = 1;
            this.calDeadline.Name = "calDeadline";
            this.calDeadline.TabIndex = 9;
            this.calDeadline.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calDeadline_DateChanged);
            // 
            // TaskListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 310);
            this.Controls.Add(this.chkFilterByDate);
            this.Controls.Add(this.btnAddTask);
            this.Controls.Add(this.btnEditTask);
            this.Controls.Add(this.btnDeleteTask);
            this.Controls.Add(this.calDeadline);
            this.Controls.Add(this.lvwTaskList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TaskListForm";
            this.Text = "Task list";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView lvwTaskList;
        private System.Windows.Forms.ColumnHeader isItDone;
        private System.Windows.Forms.ColumnHeader task;
        private System.Windows.Forms.ColumnHeader deadline;
        private System.Windows.Forms.ColumnHeader daysLeft;
        private System.Windows.Forms.CheckBox chkFilterByDate;
        private System.Windows.Forms.Button btnAddTask;
        private System.Windows.Forms.Button btnEditTask;
        private System.Windows.Forms.Button btnDeleteTask;
        private System.Windows.Forms.MonthCalendar calDeadline;
    }
}

