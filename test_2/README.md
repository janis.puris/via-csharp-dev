Vidzemes Augstskola
Inženierzinātņu fakultāte
Programmēšana II

# KD2

1.1.	 Izveido lietotni „Iepirkumu uzskaites mājsaimniecības lietotne”;
1.2.	 Lietotne ir paredzēta mājsaimniecei, kas var reģistrēt visus savus pirkumus un kontrolēt, cik naudu viņa iztērējusi ikdienas pirkumos.
1.3.	 Lietotnē jāizveido autorizēšanās lietotnē (ja realizēsi izmantojot datu bāzi nevis lokāli, lielāka atzīme user: admin; pass: admin);
1.4.	 Lietotnei ir jāvar pieslēgties lokālajai datu bāze (izveidots ar Access 2010, un lietotnē faila atrašanās vietai jābūt C:\lokal_data_base\[faila nosaukums: vards_uzvards.xxxx]);
1.5.	 Lietotnei ir jāvar ievadīt veiktos iepirkumus: Preces nosaukums, tilpus/kg, cena;
1.6.	 Izveidotos ierakstus jāvar saglabāt datu bāzē;
1.7.	 Lietotājam jāvar redzēt visus datu bāzes ierakstus;
1.8.	 Jāredz kopējā cena, kas iztērēti par pirkumiem;
1.9.	 Augstākai atzīmei:
1.9.1.	Jāvar pirkumus pārraudzīt pa datumiem (idejiski, lai redz, šīs preces pirka 20.05.2014 nākamās 21.05.2014 utt.);
1.9.2.	Preces var izdzēst, ja tika pieļauta kļūda.
1.10.	Jāizveido dizains, kas 