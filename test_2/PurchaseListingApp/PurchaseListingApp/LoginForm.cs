﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PurchaseListingApp
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            // CatalogForm catalogForm = new CatalogForm();
            // catalogForm.Show();
        }

        protected const string User = "admin";
        protected const string Pass = "admin";

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string _user = this.txtUser.Text;
            string _pass = this.txtPass.Text;

            if (_user == User && _pass == Pass)
            {
                MessageBox.Show("Access granted");
                CatalogForm catalogForm = new CatalogForm();

                this.Hide();
                catalogForm.Show();
                
            } else
            {
                MessageBox.Show("Wrong username or password. Please try again.");
                this.txtPass.Text = "";
            }
            
        }

        private void txtUser_Enter(object sender, EventArgs e)
        {
            if(txtUser.Text == "Username")
            {
                txtUser.Text = "";
                txtUser.ForeColor = Color.Black;
            }
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            if (txtPass.Text == "Password")
            {
                txtPass.Text = "";
                txtPass.ForeColor = Color.Black;
                txtPass.PasswordChar = '*';
            }
        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            if (txtUser.Text == "")
            {
                txtUser.Text = "Username";
                txtUser.ForeColor = Color.Silver;
            }
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            if (txtPass.Text == "")
            {
                txtPass.Text = "Password";
                txtPass.ForeColor = Color.Silver;
                txtPass.PasswordChar = '\0';
            }
        }
    }
}
