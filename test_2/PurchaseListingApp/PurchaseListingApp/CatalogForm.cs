﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PurchaseListingApp
{
    public partial class CatalogForm : Form
    {
        public CatalogForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            LoginForm loginForm = new LoginForm();

            this.Hide();
            loginForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dr in this.dgvPurchaseCatalog.SelectedRows)
            {
                dgvPurchaseCatalog.Rows.RemoveAt(dr.Index);
            }
        }

        private void btnSummaryRefresh_Click(object sender, EventArgs e)
        {
            double _sum = 0;
            foreach (DataGridViewRow dr in this.dgvPurchaseCatalog.Rows)
            {
                double _value;
                try
                {
                    _value = Double.Parse(Convert.ToString(dr.Cells[2].Value));
                }
                catch (Exception ex)
                {
                    _value = 0;
                    // MessageBox.Show("Warning, not all Price values are numbers!");
                }
                _sum += _value;  
            }
            this.txtTotalPrice.Text = _sum.ToString();
        }

        private void btnSaveToDb_Click(object sender, EventArgs e)
        {
            // TODO
        }
    }
}
