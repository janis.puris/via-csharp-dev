﻿namespace PurchaseListingApp
{
    partial class CatalogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPurchaseCatalog = new System.Windows.Forms.DataGridView();
            this.purchaseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volumeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSaveToDb = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.lblSummary = new System.Windows.Forms.Label();
            this.txtTotalPrice = new System.Windows.Forms.TextBox();
            this.pnlSummary = new System.Windows.Forms.Panel();
            this.btnSummaryRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseCatalog)).BeginInit();
            this.pnlSummary.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPurchaseCatalog
            // 
            this.dgvPurchaseCatalog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPurchaseCatalog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.purchaseCol,
            this.volumeCol,
            this.priceColumn});
            this.dgvPurchaseCatalog.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.dgvPurchaseCatalog.Location = new System.Drawing.Point(12, 12);
            this.dgvPurchaseCatalog.Name = "dgvPurchaseCatalog";
            this.dgvPurchaseCatalog.RowTemplate.Height = 31;
            this.dgvPurchaseCatalog.Size = new System.Drawing.Size(944, 562);
            this.dgvPurchaseCatalog.TabIndex = 0;
            // 
            // purchaseCol
            // 
            this.purchaseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.purchaseCol.HeaderText = "Purchase";
            this.purchaseCol.Name = "purchaseCol";
            // 
            // volumeCol
            // 
            this.volumeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.volumeCol.HeaderText = "Volume";
            this.volumeCol.Name = "volumeCol";
            // 
            // priceColumn
            // 
            this.priceColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.priceColumn.HeaderText = "Price";
            this.priceColumn.Name = "priceColumn";
            // 
            // btnSaveToDb
            // 
            this.btnSaveToDb.Enabled = false;
            this.btnSaveToDb.Location = new System.Drawing.Point(12, 580);
            this.btnSaveToDb.Name = "btnSaveToDb";
            this.btnSaveToDb.Size = new System.Drawing.Size(173, 79);
            this.btnSaveToDb.TabIndex = 1;
            this.btnSaveToDb.Text = "Save to DB";
            this.btnSaveToDb.UseVisualStyleBackColor = true;
            this.btnSaveToDb.Click += new System.EventHandler(this.btnSaveToDb_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 665);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(173, 79);
            this.button2.TabIndex = 2;
            this.button2.Text = "Delete selected row";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(783, 665);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(173, 79);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.Location = new System.Drawing.Point(783, 580);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(173, 79);
            this.btnLogOut.TabIndex = 4;
            this.btnLogOut.Text = "Log out";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Location = new System.Drawing.Point(3, 53);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(109, 25);
            this.lblTotalPrice.TabIndex = 5;
            this.lblTotalPrice.Text = "Total price:";
            // 
            // lblSummary
            // 
            this.lblSummary.AutoSize = true;
            this.lblSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.142858F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSummary.Location = new System.Drawing.Point(3, -1);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(103, 25);
            this.lblSummary.TabIndex = 6;
            this.lblSummary.Text = "Summary";
            // 
            // txtTotalPrice
            // 
            this.txtTotalPrice.Location = new System.Drawing.Point(118, 53);
            this.txtTotalPrice.Name = "txtTotalPrice";
            this.txtTotalPrice.ReadOnly = true;
            this.txtTotalPrice.Size = new System.Drawing.Size(118, 29);
            this.txtTotalPrice.TabIndex = 7;
            this.txtTotalPrice.Text = "0";
            // 
            // pnlSummary
            // 
            this.pnlSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSummary.Controls.Add(this.btnSummaryRefresh);
            this.pnlSummary.Controls.Add(this.txtTotalPrice);
            this.pnlSummary.Controls.Add(this.lblSummary);
            this.pnlSummary.Controls.Add(this.lblTotalPrice);
            this.pnlSummary.Location = new System.Drawing.Point(526, 580);
            this.pnlSummary.Name = "pnlSummary";
            this.pnlSummary.Size = new System.Drawing.Size(251, 164);
            this.pnlSummary.TabIndex = 8;
            // 
            // btnSummaryRefresh
            // 
            this.btnSummaryRefresh.Location = new System.Drawing.Point(3, 100);
            this.btnSummaryRefresh.Name = "btnSummaryRefresh";
            this.btnSummaryRefresh.Size = new System.Drawing.Size(243, 59);
            this.btnSummaryRefresh.TabIndex = 8;
            this.btnSummaryRefresh.Text = "Refresh";
            this.btnSummaryRefresh.UseVisualStyleBackColor = true;
            this.btnSummaryRefresh.Click += new System.EventHandler(this.btnSummaryRefresh_Click);
            // 
            // CatalogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 756);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnSaveToDb);
            this.Controls.Add(this.dgvPurchaseCatalog);
            this.Controls.Add(this.pnlSummary);
            this.Name = "CatalogForm";
            this.Text = "Purchases";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseCatalog)).EndInit();
            this.pnlSummary.ResumeLayout(false);
            this.pnlSummary.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPurchaseCatalog;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchaseCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn volumeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceColumn;
        private System.Windows.Forms.Button btnSaveToDb;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.Label lblSummary;
        private System.Windows.Forms.TextBox txtTotalPrice;
        private System.Windows.Forms.Panel pnlSummary;
        private System.Windows.Forms.Button btnSummaryRefresh;
    }
}